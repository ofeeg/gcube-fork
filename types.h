#ifndef __TYPES_H
#define __TYPES_H 1


typedef __signed__ char sig8;
typedef unsigned char usig8;

typedef __signed__ short sig16;
typedef unsigned short usig16;

typedef __signed__ int sig32;
typedef unsigned int usig32;

typedef __signed__ long long sig64;
typedef unsigned long long usig64;


#endif // __TYPES_H
