#ifndef __DISKIO_H
#define __DISKIO_H 1


#include <stdio.h>
#include <string.h>
#include <zlib.h>
#include "elf.h"

#include "general.h"
#include "gdebug.h"
#include "mem.h"

#define DOL_NTEXT 7
#define DOL_NDATA 11

#define GCS_COMPRESSED_GZ				0x01

#define GC_FILE_ERROR						0xffffffff

// text - code (read-only)
// data - data (read-write)
// bss - uninitialized data (read-write)

typedef struct
{
	usig32 text_offset[7];
	usig32 data_offset[11];

	usig32 text_address[7];
	usig32 data_address[11];

	usig32 text_size[7];
	usig32 data_size[11];

	usig32 bss_address;
	usig32 bss_size;

	usig32 entry_point;

	usig32 padd[7];
} DOLHeader;


typedef struct
{
	usig8  magic[4];
	usig32 version;
	usig32 flags;
	usig32 reserved[15];

	// filename of the gcm (if any)
	// must be in the same dir as savestate
	char filename[256];
	// yet to come
	usig32 screenshot_size;
	usig32 screenshot_offset;

	// block size and position in file
	usig32 mem_size;
	usig32 mem_offset;

	usig32 l2c_size;
	usig32 l2c_offset;

	usig32 aram_size;
	usig32 aram_offset;
	
	usig32 regs_size;
	usig32 regs_offset;
	usig32 ps0regs_size;
	usig32 ps0regs_offset;
	usig32 ps1regs_size;
	usig32 ps1regs_offset;

	usig32 dspregs_size;
	usig32 dspregs_offset;

	usig32 airegs_size;
	usig32 airegs_offset;

	usig32 cpregs_size;
	usig32 cpregs_offset;

	usig32 diregs_size;
	usig32 diregs_offset;

	usig32 eiregs_size;
	usig32 eiregs_offset;

	usig32 miregs_size;
	usig32 miregs_offset;

	usig32 peregs_size;
	usig32 peregs_offset;

	usig32 piregs_size;
	usig32 piregs_offset;

	usig32 siregs_size;
	usig32 siregs_offset;

	usig32 viregs_size;
	usig32 viregs_offset;
	
	usig32 gxstate_size;
	usig32 gxstate_offset;

	usig32 dspstate_size;
	usig32 dspstate_offset;
} GCSHeader;


#define IMP_MAGIC						"\0x7fIMP"

typedef struct IMPMarkTag
{
	usig32 offset;
	usig32 length;

	struct IMPMarkTag *next;
} IMPMark;

typedef struct
{
	char magic[4];
	usig32 version;
	usig32 nitems;
	usig8 reserved[4];
} IMPHeader;

#define FILE_IMP				1

typedef struct
{
	FILE *f;

	IMPMark *marks, *current_mark;

	unsigned int zero_offset;
	int compressed_offset;
	unsigned int pos;

	usig8 data;
} IMPFile;

typedef struct
{
	char *filename;
	int type;

	void *f;
} File;


usig32 load_bin (char *filename);
usig32 load_dol (char *filename);
int is_elf (char *filename);
usig32 load_elf (char *filename);

int is_gcs (char *filename);
void save_state (char *filename, int compressed);
usig32 load_state (char *filename);

usig32 load_gc (char *filename);

int is_imp (char *fname);
void imp_close (IMPFile *file);
IMPFile *imp_open (char *fname);
int imp_seek (IMPFile *file, int offset, int whence);
int imp_read (IMPFile *file, char *buff, unsigned int size);

File *file_open (char *filename);
void file_close (File *file);
int file_seek (File *file, int offset, int whence);
int file_tell (File *file);
int file_read (File *file, void *buff, unsigned int size);
usig32 file_read_w (File *file);


#endif // __DISKIO_H
