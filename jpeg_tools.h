#ifndef __JPEG_TOOLS_H
#define __JPEG_TOOLS_H 1


#include "general.h"


void jpeg_decompress (usig8 *src, usig32 size, char *dst, int pitch);


#endif // __JPEG_TOOLS_H
