#ifndef __HW_H
#define __HW_H 1


#include <stdio.h>
#include <string.h>

#include "general.h"
#include "hw_ai_dsp.h"
#include "hw_cp.h"
#include "hw_di.h"
#include "hw_exi.h"
#include "hw_gx.h"
#include "hw_mi.h"
#include "hw_pe.h"
#include "hw_pi.h"
#include "hw_si.h"
#include "hw_vi.h"

#include "mem.h"

void hw_init (void);
void hw_reinit (void);
void hw_set_video_mode (int country_code);

int hw_rword (usig32 address, usig32 *data);
int hw_rhalf (usig32 address, usig16 *data);
int hw_wword (usig32 address, usig32 data);
int hw_whalf (usig32 address, usig16 data);


#endif // __HW_H
