#ifndef __GX_TRANSFORM_H
#define __GX_TRANSFORM_H 1



inline usig32 color_unpack_rgb565 (usig32 X);
inline usig32 color_unpack_rgba4 (usig32 X);
inline usig32 color_unpack_rgba6 (usig32 X);
inline usig32 color_unpack_rgb555 (usig32 X);
inline usig32 color_unpack_rgb4a3 (usig32 X);
inline usig32 color_unpack_rgb5a3 (usig32 X);
inline usig32 color_unpack_i4 (usig32 X);
inline usig32 color_unpack_i8 (usig32 X);
inline usig32 color_unpack_ia4 (usig32 X);
inline usig32 color_unpack_ia8 (usig32 X);

unsigned int gx_draw (usig32 mem, int prim, int n, int vat);


#endif // __GX_TRANSFORM_H
