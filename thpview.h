#ifndef __THPVIEW_H
#define __THPVIEW_H 1


#include <stdio.h>
#include <errno.h>
#include <SDL/SDL.h>

#include "jpeg_tools.h"
#include "general.h"
#include "mem.h"

#define THPVIEW_VERSION					"0.1"


#define THP_MAGIC 0x54485000
#define COMPONENT_VIDEO_ID			0
#define COMPONENT_AUDIO_ID			1
#define COMPONENT_NONE_ID				255

#define COMPONENT_VIDEO_SIZE(X)		((THP_VERSION (X) == 0x00010000) ? 8 : 12)
#define COMPONENT_AUDIO_SIZE(X)		((THP_VERSION (X) == 0x00010000) ? 12 : 16)

#define THP_HEADER(X)							((THPHeader *) X)
#define THP_AUDIO_PRESENT(X)			(THP_HEADER (X)->max_audio_samples)
#define THP_VERSION(X)						(BSWAP32 (THP_HEADER (X)->version))
#define THP_COMPONENT_DATA(X)			((THPComponentData *) (X + BSWAP32 (THP_HEADER (X)->component_data_offset)))
#define THP_COMPONENTS_INFO(X)		((void *) (sizeof (THPComponentData) + (void *) THP_COMPONENT_DATA (X)))
#define THP_VIDEO_INFO(X)					((THPVideoInfo *) (sizeof (THPComponentData) + (void *) THP_COMPONENT_DATA (X)))
#define THP_AUDIO_INFO(X)					((THPAudioInfo *) (THP_AUDIO_PRESENT (X) ? (((THP_VERSION (X) == 0x00010000) ? 8 : 12) + (void *) THP_VIDEO_INFO (X)) : NULL))

#define JPEG_SOI_MARKER 0xffd8
#define JPEG_APP0_MARKER 0xffe0
#define JPEG_SOS_MARKER 0xffda
#define JPEG_EOI_MARKER 0xffd9

#define SOI_MARKER 0xd8
#define SOS_MARKER 0xda
#define EOI_MARKER 0xd9


typedef struct
{
	usig32 magic;
	usig32 version;
	usig32 max_buffer_size;
	usig32 max_audio_samples;
	float framerate;
	usig32 num_frames;
	usig32 first_frame_size;
	usig32 movie_data_size;
	usig32 component_data_offset;
	usig32 offsets_data_offset;
	usig32 first_frame_offset;
	usig32 last_frame_offset;
} THPHeader;


typedef struct
{
	usig32 num_components;
	usig8 components[16];
} THPComponentData;


typedef struct
{
	usig32 width;
	usig32 height;
	usig32 reserved; // for version 1.1
} THPVideoInfo;


typedef struct
{
	usig32 num_channels;
	usig32 frequency;
	usig32 num_samples;
	usig32 num_data;	// for version 1.1
} THPAudioInfo;


typedef struct
{
	usig32 last_frame_size;
	usig32 next_frame_size;
	usig32 video_data_size;
	usig32 audio_data_size;
	// other components sizes
} THPFrameHeader;


struct JPEGHeaderTag
{
	usig16 length;
	char magic[5];
	usig16 version;
	usig8 units;
	usig16 Xdensity, Ydensity;
	usig8 Xthumbnail, Ythumbnail;
} __attribute__ ((packed));
typedef struct JPEGHeaderTag JPEGHeader;


typedef struct
{
	usig8 *data, *end_of_header;
	usig32 size;
} JPEGFrame;

typedef struct
{
	usig8 *data;
	usig32 size;
} AUDIOFrame;


#endif // __THPVIEW_H
