#ifndef __GX_TEXTURE_H
#define __GX_TEXTURE_H 1


#define TEXCACHE_MAGIC				BSWAP32 (0xdeadbeef)
#define XFB_MAGIC							BSWAP32 (0xbabe1ee5)
#define MAX_TEXCACHE_TAGS				0x1000


typedef struct
{
	unsigned int format, type, internal_format;
} TexFormat;


typedef struct
{
	usig32 address;
	usig32 tlut_address;
	unsigned int tex;
	unsigned int type;

	unsigned int p2;
	int reload;
	
	int width, height;
	int format;
	int tlut_format;
	
	usig32 marker_address;
	usig32 marker_save;

	usig32 misses;
	unsigned int size;
	
	int mipmap;
	int mipmaps_loaded;
	usig32 even_lod;
	
	// for render targets
	int xfb_mipmap;
	int dont_use;
} TextureTag;


typedef struct
{
	TextureTag tags[MAX_TEXCACHE_TAGS];
	unsigned int ntags;

	usig32 memory_used;
} TextureCache;


void gx_convert_texture_i4_rgba (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_i4 (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_i8_rgba (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_i8 (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_ia4_rgba (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_ia4 (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_ia8_rgba (usig8 *data, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_ia8 (usig8 *data, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_rgb565_rgba (usig8 *data, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_rgb565 (usig8 *data, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_rgb5a3_rgba (usig8 *data, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_rgba8_rgba (usig8 *data, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_ci4_rgba (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_ci8_rgba (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_ci14x2_rgba (usig8 *data, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_cmp_rgba (usig8 *data, int width, int height, usig16 *tlut, int format, TexFormat *tf);
void gx_convert_texture_i4_rgba (usig8 *src, int width, int height, usig16 *tlut, int format, TexFormat *tf);
char *gx_convert_texture (usig8 *src, int width, int height, int format, sig16 *tlut, int tlut_format);

usig32 gl_texture_calculate_size (usig32 npixels, unsigned int gl_internal_format);
usig32 gx_texture_calculate_size (usig32 width, usig32 height, unsigned int format);

void texcache_tlut_reload (usig32 tlut_address);
int texcache_tag_valid (TextureTag *tag);
void texcache_tag_validate_fast (TextureTag *tag);
void texcache_tag_validate (TextureTag *tag);
void texcache_tag_invalidate (TextureTag *tag);
void texcache_invalidate_all (void);
void texcache_remove_tag (TextureTag *tag);
void texcache_remove_all (void);
void texcache_remove_unused (void);
TextureTag *texcache_add_tag (usig32 address, usig32 tlut_address,
		unsigned int tex, unsigned int type, int width, int height,
		int format, int tlut_format, unsigned int gl_internal_format,
		int mipmap, int min_lod, int max_lod,	usig32 even_lod);
TextureTag *texcache_fetch (usig32 address, int width, int height);

void gx_enable_texture (unsigned int index, int enable);
void gx_load_texture (unsigned int index);
void gx_dump_active_texture (int index, int lod);
void gx_create_render_target (void);
void gx_render_to_texture (usig32 address, unsigned int x, unsigned int y,
													 unsigned int w, unsigned int h, int mipmap);


#endif // __GX_TEXTURE_H
