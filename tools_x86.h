#ifndef __TOOLS_X86_H
#define __TOOLS_X86_H 1


// GNU AS inline routines

static inline __attribute__ ((const)) usig16 BSWAP_16 (usig16 X)
{
	asm
	(
		"xchgb %b0, %h0"
		: "=q" (X)
		: "0" (X)
	);
	
	return X;
}


static inline __attribute__ ((const)) usig32 BSWAP_32 (usig32 X)
{
	asm
	(
		"bswap %0"
		: "=r" (X)
		: "0" (X)
	);
	
	return X;
}


static inline __attribute__ ((const)) usig64 BSWAP_64 (usig64 X)
{
	return (((usig64)(BSWAP_32 (X)) << 32) | (BSWAP_32 (X >> 32)));
}


static inline __attribute__ ((const)) usig32 ROTL (usig32 X, int n)
{
	asm
	(
		"roll %b2, %0"
		: "=g" (X)
		: "0" (X), "cI" (n)
	);

	return X;
}


static inline __attribute__ ((const)) int CARRY (usig32 A, usig32 B)
{
	register usig8 carry;


	asm
	(
		"addl %2, %1"
		"setc %0"
		: "=&q" (carry), "+&r" (A)
		: "g" (B)
	);
	
	return carry;
}


static inline __attribute__ ((const)) int CARRY3 (usig32 A, usig32 B, usig32 C)
{
	register usig8 carry;


	asm
	(
		"addl %2, %1\n"
		"setc %0\n"
		"addl %3, %1\n"
		"adcb $0, %0"
		: "=&q" (carry), "+&r" (A)
		: "g" (B), "g" (C)
	);
	
	return carry;
}


static inline __attribute__ ((const)) int BITSCAN (usig32 X)
{
	asm
	(
		"bsr %0,%0"
		: "=r" (X)
		: "0" (X)
	);
	
	return 31 - X;
}


#endif // __TOOLS_X86_H
